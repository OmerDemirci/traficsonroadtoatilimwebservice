package tort.atilim.webservice;

import java.util.Hashtable;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

public class EventDetails  {
	int longtitude;
	int latitude;
	String type;
	String submitter;
	EventDetails(int longtitude,int latitude,String type,String submitter){
		this.longtitude=longtitude;
		this.latitude=latitude;
		this.type=type;
		this.submitter=submitter;
	}
	public EventDetails() {
		// TODO Auto-generated constructor stub
	}
	public int getLongtitude() {
		return longtitude;
	}
	public void setLongtitude(int longtitude) {
		this.longtitude = longtitude;
	}
	public int getLatitude() {
		return latitude;
	}
	public void setLatitude(int latitude) {
		this.latitude = latitude;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getSubmitter() {
		return submitter;
	}
	public void setSubmitter(String submitter) {
		this.submitter = submitter;
	}
}
