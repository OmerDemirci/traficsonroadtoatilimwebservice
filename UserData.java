package tort.atilim.webservice;

public class UserData {
  private String username;
  private String password;
  private String name;
  private String surname;
  private String squestion;
  private String email;
  private String sanswer;
  public UserData(String username,String password,String name,String surname,String squestion
		  ,String sanswer,String email){
	  this.username=username;
	  this.password=password;
	  this.name=name;
	  this.surname=surname;
	  this.squestion=squestion;
	  this.email=email;
	  this.sanswer=sanswer;
  }
public String getUsername() {
	return username;
}
public void setUsername(String username) {
	this.username = username;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getSurname() {
	return surname;
}
public void setSurname(String surname) {
	this.surname = surname;
}
public String getSquestion() {
	return squestion;
}
public void setSquestion(String squestion) {
	this.squestion = squestion;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getSanswer() {
	return sanswer;
}
public void setSanswer(String sanswer) {
	this.sanswer = sanswer;
}
  
}
