package tort.atilim.webservice;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import org.ksoap2.serialization.SoapObject;

public class Functions {
	private  Connection con;
	private PreparedStatement statement;
	private ResultSet selectRecord;
	private ArrayList<EventDetails> eventData = new ArrayList<EventDetails>();
	public String UserNameChecker(String username){
		UserData usr;
		try{
			String dataUsr="";
			statement=con.prepareStatement("SELECT * FROM user where username='"+username+"'");
			selectRecord=statement.executeQuery();
			while(selectRecord.next())
			{
				 dataUsr=selectRecord.getString("username");
				
			} 
			return dataUsr;
		}
		catch(Exception e){
			   e.printStackTrace(); 
			  } 
		return username;
	 }
	 public void dataOpenner(){
		 
		 try{
			  Class.forName("com.mysql.jdbc.Driver").newInstance();

		   con = DriverManager.getConnection("jdbc:mysql://localhost/TraficEvents","root","");
		   
		 }
		 catch(Exception e){
			   e.printStackTrace(); 
			  } 
		 
	 }
	 public void databaseCloser(){
			try{
				if(statement!=null)
				{
				statement.close();
				}

				if(con!=null)
				{
					con.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace(); 
			}
		}
	public String authentication(String userName,String password){
		   
		  String retrievedUserName = null;
		  String retrievedPassword = null;
		  String status = "";
		  dataOpenner();
		  try{
			   statement =  con.prepareStatement("SELECT * FROM user WHERE username = '"+userName+"'");
			   ResultSet result = statement.executeQuery();
		    
			   while(result.next()){
				   	retrievedUserName = result.getString("username");
				   	retrievedPassword = result.getString("password");
			   }
			   		if(retrievedUserName!=null&&retrievedPassword!=null){
			   			if(retrievedUserName.equals(userName)&&retrievedPassword.equals(password)){
			   				status = "Success!";
			   			}
			   			else{
			   				status = "Login fail!!!";
			   			}
			   		}
			   		else{
			   			status = "Login fail!!!";
			   		}
		   	
		  }
		  catch(Exception e){
			   e.printStackTrace(); 
			  }
		  databaseCloser();
		  return status;
		  
		 }
	public String insertToUser(String name,String surname,String username,String password,
			String email,String squestion,String sanswer){
		dataOpenner();
		String dataUsr=UserNameChecker(username);
		if(!dataUsr.equals(username)){
			 try{
			 
				 statement =  con.prepareStatement("insert into user (name," +
			  	"surname,username,password,squestion,sanswer,email) values(?,?,?,?,?,?,?)");
		
			 	statement.setString(1, name);
			 	statement.setString(2, surname);
			 	statement.setString(3, username);
			 	statement.setString(4, password);
			 	statement.setString(5, squestion);
			 	statement.setString(6, sanswer);
			 	statement.setString(7, email);
			 	statement.executeUpdate();
			 	databaseCloser();
			 	return "User information successfuly added to database";
			 
		 }
			 catch(Exception e)
			 {
			 e.printStackTrace();
			 return "Database error occured ";
			 }
			 
		 }
		
		else 
			return "Username on use";
		 
		
	}
	public void insertToEvent(String eventType,String submitter,int longtitude,int latitude){
		dataOpenner();
		
			 try{
			 
				 statement =  con.prepareStatement("insert into event (type," +
			  	"submitter) values(?,?)");
		
			 	statement.setString(1,eventType);
			 	statement.setString(2,submitter);
			 	statement.executeUpdate();
			 	statement =  con.prepareStatement("SELECT e_id FROM event ORDER BY e_id DESC LIMIT 0,1");
				   ResultSet result = statement.executeQuery();
			    int id=0;
				   while(result.next()){
					   id = result.getInt("e_id");
				   }
				   
				   statement =  con.prepareStatement("insert into cordinates (id," +
						  	"longtitude,latitude) values(?,?,?)");
				   statement.setInt(1,id);
				   statement.setInt(2,longtitude);
				   statement.setInt(3,latitude );
				   statement.executeUpdate();
			 	databaseCloser();
			 
			 
		 }
			 catch(Exception e)
			 {
			 e.printStackTrace();
			 
			 }

	}
	public ArrayList<EventDetails> getEventList(){
		dataOpenner();
		  try{
			   statement =  con.prepareStatement("SELECT * FROM `cordinates` c, `event` e WHERE c.id = e.e_id;");
			   ResultSet result = statement.executeQuery();
			   EventDetails details;
			   while(result.next()){
				   	int longtitude = result.getInt("longtitude");
				   	int latitude = result.getInt("latitude");
				   	String type = result.getString("type");
				   	String submitter = result.getString("submitter");
				   	details=new EventDetails(longtitude,latitude,type,submitter);
				   	eventData.add(details);
			   }
			   		
		   	
		  }
		  catch(Exception e){
			   e.printStackTrace(); 
			  }
		  databaseCloser();
		return eventData; 
			 

	}
	public static EventDetails[] RetrieveFromSoap(SoapObject soap)
    {
		EventDetails[] categories = new EventDetails[soap.getPropertyCount()];
        for (int i = 0; i < categories.length; i++) {
            SoapObject pii = (SoapObject)soap.getProperty(i);
            EventDetails category = new EventDetails();
            category.longtitude = Integer.parseInt(pii.getProperty(0).toString());
            category.latitude = Integer.parseInt(pii.getProperty(1).toString());
            category.type = pii.getProperty(2).toString();
            category.submitter = pii.getProperty(3).toString();
            categories[i] = category;
        }
        return categories;
    }
}
